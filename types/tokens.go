package types

const (
	NativeTokenSymbol = "DEE"

	TokenMaxTotalSupply int64 = 9000000000000000000 // 90 billions with 8 decimal digits
)
